# -*- coding: utf-8 -*-
"""
Functions for reading Pekin Elmer UV/Vis spectroscopic data provided by UV WinLab 
software
"""
#M.Rabe , 2018

import numpy as _np
from astropy.io import ascii as _ascii
from specage.spcl import Spectrum



def singleascii(Filename):
    """Return Spectrum object from UVWinlab ascii file"""
    datatable = _ascii.read(Filename, data_start=86, delimiter='\s')        
    nm = _np.array(datatable.columns[0])
    au = _np.array(datatable.columns[1])
    return Spectrum(nm, au,FileName=Filename, ylabel='absorbance / au',
                         xlabel='\lambda / nm')

def filelist(Filelist):
    """Return list of Spectrum object from list of UVWinlab ascii files"""
    Speclist=[]
    for File in Filelist:
        Speclist.append(singleascii(File))
    return Speclist
