# -*- coding: utf-8 -*-
"""
Functions to create read ASCII files exported from LabRam Raman spectrometer
"""
#M.Rabe , 2016

import os    


from specage.spcl import Spectrum, TimeSeries
from specage.ascii_magic import readASCII_table
import datetime as dt


def multi_labramASCII(filelist):
    """Read multiple ASCII files created by Witecs 'Project Five' software and 
    return list of Spectrum objects"""
    outputlist=[]
    for file in filelist:
        outputlist.append(labramASCII(file))
    return outputlist
    
def labramASCII(file):
    """Read single ASCII file created by Witecs 'Project Five' software and 
    return a Spectrum object"""
    curMat, header = readASCII_table(file, headlines=0, delim = '\t')
    spec = Spectrum(curMat[:,0], curMat[:,1], 
                             xlabel='\wavenumber / nm', ylabel='counts',
                             FileName=os.path.split(file)[1])

    return spec
