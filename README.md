# specage

Welcome to specage. The Python library for basic data interpretation in optical 
spectroscopy, with broad applicability (IR, Vis, UV, Fluorescence,...).

specage is intended to be a basic package that allows to apply common methods 
such as base line operations peak -find or -fit, integration and others to 
optical spectroscopic data sets. 

The actual acquisition or sampling method is only important for the data 
reading part which in this package is achieved by specific reader functions 
("readers"). The task of the reader is to create spectral objects (Spectrum, 
TimeSeries, SpectralSum) provided by the specage.spcl library. The spectral 
classes provide the methods commonly applied to all sorts of optical 
spectroscopy.

At the moment readers for spectroscopic instruments in the Interface 
Spectroscopy group of the MPIE are available. For specage to become a useful 
tool for many people it is desired that people write and share their own 
readers and methods for the spectral classes. In case you miss a method or 
feature and do not know how to add it to specage or any other questions or
suggestions please contact Martin Rabe.


Installation in a nutshell
==========================

There is no installer yet, feel free to write one, otherwise you may:

 1. Install Python3

 2. Install Python packages astropy, numpy, matplotlib, uncertainties (spc)

 3. Clone or download specage

 4. Add the path containing specage to your PYTHONPATH

 5. optional: look at the examples folder

Examples
========

Currently there are two examples that demonstrate the usage of specage:

 1. The basic example showing basic functionalities: [TS_example](/examples/TS_example.ipynb)
 2. The peak fitting example: [example_pkfit](/examples/example_pkfit.ipynb)
