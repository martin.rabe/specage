# -*- coding: utf-8 -*-
"""
Readers for files created Brukers and BioRads IR spektrometer software
"""
#M.Rabe , 2017-21
#TODO: Maybe at some point we may get rid of astropy dependency
import os

import numpy as _np
from astropy.io import ascii as _ascii
from specage.ascii_magic import readASCII_table  as _readascii
from specage.spcl import Spectrum

def bruk_multidpt(filelist):
    """Read multiple dpt files created by Brukers OPUS software and return list 
    of Spectrum objects"""
    output=[]
    for argv in filelist: #find .dpt files and call bruk_dpt function
        if os.path.exists(argv):
            if os.path.isfile(argv) & (str(argv).endswith('.dpt') 
            | str(argv).endswith('.txt')):
                output.append(bruk_dpt(argv))
            
            #if os.path.isdir(argv):
            #    files=os.listdir(argv)
            #    print('a directory')
            #    for f in files:
            #        if os.path.isfile(f) & str(f).endswith('.dpt'):
            #            output.append(readit(f))
                            
    if len(output) != 0:
        return output
    else:
        print('No .dpt files found in {}'.format(str(filelist)))
    
def bruk_dpt(dptfile):
    """Read single dpt file created by Brukers OPUS software and return a
    Spectrum object"""
    data = _ascii.read(dptfile)        
    wavenum = _np.array(_np.float64(data.columns[0]))
    Int =  _np.array(_np.float64(data.columns[1]))
    res = Spectrum(wavenum, Int, xlabel='wavenumber', ylabel='absorbance',
                        FileName=os.path.split(dptfile)[1])
    
    return res

def biorad_csv(csvfile):
    """Read single csv file created by BioRads software and return a
    Spectrum object"""
    datatable = _readascii(csvfile, headlines=5, delim = ',')
    wvl = _np.array(datatable[0][:,0])
    Abs = _np.array(datatable[0][:,1])
    xl = datatable[1][0][7:]   
    yl = datatable[1][1][7:]   
    return Spectrum(wvl, Abs,FileName=csvfile, ylabel=yl,
                    xlabel=xl, comment= datatable[1][2:])


def biorad_multicsv(Filelist):
    """Return list of Spectrum s from list of csv files from Boprad software"""
    Speclist=[]
    for File in Filelist:
        Speclist.append(biorad_csv(File))
    return Speclist
    
def AFMIR_csv(csvfile):
    """Reads single csv file created by AFMIR and returns a list of Spectrum 
    objects.    """
    datatable = _readascii(csvfile, headlines=1, delim=',')
    wvl = _np.array(datatable[0][:,0])
    spectralist = []
    
    for col in _np.arange(1,datatable[0].shape[1]):
        Abs = _np.array(datatable[0][:,col])
        spectralist.append(Spectrum(wvl, Abs, FileName='', ylabel='IR-Amplitude',
                  xlabel='wavenumber', comment=''))
    return spectralist