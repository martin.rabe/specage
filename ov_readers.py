# -*- coding: utf-8 -*-
"""
Readers for ASCII spectra files created by OceanOptics OveanView (R)
"""
#M.Rabe , 2018

# python libraries:
import datetime as _dt
import locale as _lc

# third party libraries:
from astropy.io import ascii as _ascii

from specage.spcl import Spectrum, TimeSeries
from specage.ascii_magic import find_line, find_str_in_line, readlines


def spectra_time_series(filename):
    """Read Time Series of spectra and returns 'TimeSeries' object."""
    # read in wavelength
    ds = find_line(filename, '>>>>>Begin Spectral Data<<<<<')
    wvlgth = _ascii.read(filename, data_start=ds[0], data_end=ds[0]+1, delimiter='\s')
    wl=[]
    for wvlstr in wvlgth[0]:
        
        try:
            wl.append(float(wvlstr.replace(',','.')))
        except AttributeError:
            wl.append(float(wvlstr))

    #read in spectra
    datatable = _ascii.read(filename, data_start=ds[0]+1, delimiter='\s')
    speclist=[];
    for index, line in enumerate(datatable):
        tstamp=_dt.datetime.strptime('{} {} {}'.format(line[0],line[1][:-7],
                                    line[1][-6:]),'%Y-%m-%d %H:%M:%S %f')
        values = []
        for valind in range(3,len(line)):
            values.append(float(line[valind].replace(',','.')))

        speclist.append(Spectrum(wl, values, t=tstamp, 
                                 xlabel='wavelength / nm', 
                                 ylabel='intensity'))
        
    ts = TimeSeries(speclist, FileNames=filename)
    return ts


def single_spectrum(filename):
    """Read single spectrum file and return Spectrum object."""
    ds = find_line(filename, '>>>>>Begin Spectral Data<<<<<')
    datatable = _ascii.read(filename, data_start=ds[0], delimiter='\s')
    
    wvlgth = []
    intensity = []
    for item in datatable:
        wvlgth.append(float(item[0].replace(',','.')))
        intensity.append(float(item[1].replace(',','.')))
    dateline = readlines(filename, find_str_in_line(filename, 'Date:'))[0][6:-1]
    #workaround for reading out the correct datestring seems to cause problems 
    #on windows machines needs to be tested/improved:
    oldlocale = _lc.setlocale(_lc.LC_TIME)
    if oldlocale != 'C':
        _lc.setlocale(_lc.LC_TIME, 'en_US.UTF8');
        
    rectime = _dt.datetime.strptime(dateline, '%a %b %d %H:%M:%S %Z %Y')
    _lc.setlocale(_lc.LC_TIME, oldlocale);
    return Spectrum(wvlgth, intensity, t=rectime, xlabel='wavelength / nm', 
                    ylabel='intensity')    
    
    


def time_trend(filename):
#    data = _ascii.read(
#            filename,
#            data_start = find_line(filename, '>>>>>Begin Spectral Data<<<<<\n'), 
#            delimiter='\s'
#            )
#    return data
    pass

    