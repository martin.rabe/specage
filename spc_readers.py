# -*- coding: utf-8 -*-
"""
Functions to create spectrum and TimeSeries objects from Input files
Reading .spc files needs spc module: https://github.com/rohanisaac/spc.git
"""
#M.Rabe , 2016

import os    

import spc

from specage.spcl import Spectrum, TimeSeries

import datetime as dt


def spc2ts(SpcFile):
    """ Return spectral time series from single spc file containing several 
        spectra. """
    f = spc.File(SpcFile);
    spectra = []  
    datafiletime = dt.datetime(f.year,f.month, f.day, f.hour, f.minute)
    for Spec in f.sub:
        # Attempt to determine time unit
        if 'sec' in f.zlabel or 'Sec' in f.zlabel or 's' is f.zlabel:
            tstamp = datafiletime + dt.timedelta(seconds=Spec.subtime)
        elif 'min' in f.zlabel or 'Min' in f.zlabel or 'm' is f.zlabel:
            tstamp = datafiletime + dt.timedelta(minutes=Spec.subtime)
        elif 'hours' in f.zlabel or 'Hours' in f.zlabel or 'h' is f.zlabel:
            tstamp = datafiletime + dt.timedelta(hours=Spec.subtime)
            
        spectra.append (Spectrum(f.x, Spec.y, 
                                xlabel=f.xlabel, ylabel=f.ylabel,
                                t=tstamp))
        
    TS = TimeSeries(spectra, FileNames=[SpcFile])
    
    return TS


def tslist(SPClist):
    """ Return List of TimeSeries from list of spc files"""
    TSList = [];
    for FileIdx, FullName in enumerate(SPClist):
        
        PathName, FileName = os.path.split(FullName)             
        print('\n reading File: {} \n'.format(FileName))        
        
        TSList.append(spc2ts(FullName))
        
    return TSList

