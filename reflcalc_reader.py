# -*- coding: utf-8 -*-
"""
Functions for reading reflcalcs *.out files.
Currently only RA files are supported.

"""

from specage.ascii_magic import readASCII_table  as _readascii
from specage.spcl import Spectrum


def RAspectrum(outfile, argument):
    """Return Spectrum object from reflcalc .out reflectance absorbanc file.
        
    outfile: str that represents path to *.out file
    argument: str polarisation that was used as 3rd input argument to reflcalc 
               ('p', 's', or 'a') for argument = 'a' a list of spectra is 
              returned"""
        
    data, header=_readascii(outfile)
    if argument == 'p':
        sp = Spectrum(data[:,0], data[:,1], xlabel='wavenumber [cm^-^1]', 
                      ylabel = 'RA', comment = 'p')
        sp.pol = 'p'
        return sp
    
    elif argument == 's':
        sp = Spectrum(data[:,0], data[:,1], xlabel='wavenumber [cm^-^1]', 
                      ylabel = 'RA', comment = 's')
        sp.pol = 's'
        return sp
    
    elif argument == 'a':
        # reflcalc returns 2 spectra in a 2 column table with option a. 
        # Currently it is not clear what is what
        d = data.T.reshape(4,-1).T #divide output in single specs
        
        sp=[]
        specorder = ['p','s']
        
        for nr in range(2):
            sp.append(Spectrum(d[:,nr], d[:,nr+2], 
                               xlabel='wavenumber [cm^-^1]', 
                               ylabel = 'RA', comment = specorder[nr]))
            sp[nr].pol = specorder[nr]
        return sp
    else:
        raise ValueError('argument must be "s", "p", or "a"')
            
# ---------------------- some code for testing --------------------------

if __name__ == '__main__':
    pass


        