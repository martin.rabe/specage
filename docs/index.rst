.. specage documentation master file, created by
   sphinx-quickstart on Wed Apr  4 13:53:03 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the specage documentation!
======================================

Introduction
------------

Welcome to specage. The Python library for basic data interpretation in optical 
spectroscopy, with broad applicability (IR, Vis, UV, Fluorescence,...).

specage is intended to be a basic package that allows to apply common methods 
such as base line operations peak -find or -fit, integration and others to 
optical spectroscopic data sets. 

The actual acquisition or sampling method is only important for the data 
reading part which in this package is achieved by specific reader functions 
("readers"). The task of the reader is to create spectral objects (Spectrum, 
TimeSeries, SpectralSum) provided by the specage.spcl library. The spectral 
classes provide the methods commonly applied to all sorts of optical 
spectroscopy.

At the moment readers for spectroscopic instruments in the Interface 
Spectroscopy group of the MPIE are available. For specage to become a useful 
tool for many people it is desired that people write and share their own 
readers and methods for the spectral classes. In case you miss a method or 
feature and do not know hoe to add it to specage or any other questions or
suggestions please contact Martin Rabe.


Installation in a nutshell:

There is no installer yet, feel free to write one, otherwise you may:
 1. Install Python3
 2. Install Python packages astropy, numpy, matplotlib, (spc)
 3. Clone or download specage
 4. Add the path containing specage to your PYTHONPATH
 5. optional: look at the example.py


Classes
--------

.. automodule:: specage.spcl
   :members:

Modules for Spectral Analysis
-------------------------------

ascii_magic module:

	.. automodule:: specage.ascii_magic
		:members:
   
peakfit module:

	.. automodule:: specage.peakfit
		:members:


unit_convert module:
	.. automodule:: specage.unit_convert
		:members:

Readers
---------

IR-readers:

    .. automodule:: specage.ir_readers
       :members:

OceanOptics readers:

    .. automodule:: specage.ov_readers
       :members:

Raman readers:
    
    .. automodule:: specage.labram_readers
       :members:

    .. automodule:: specage.witec_readers
       :members:   

Perkin-Elmer UV/Vis readers

    .. automodule:: specage.peuvvis_readers
       :members:
	   
Special software readers:

    .. automodule:: specage.wvr_reader
       :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
