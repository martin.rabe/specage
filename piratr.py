#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 09:49:28 2018
Modul for interpretation of polarized IR ATR spectra. Formulas are based on: 
    N. J. Harrick, 1987, Internal Reflection Spectroscopy.
    U. P. Fringeli in Internal Reflection Spectroscopy Theory and Applications, 
    F. M. Mirabella ed., 1993, pp255-324
@author: Martin Rabe
"""
import specage.unit_convert as _uc
import numpy as _np
from numpy import pi, inf#, sin, arcsin, cos, sqrt, exp
#import the math functions from the uncertainties.unumpy package 
from uncertainties.unumpy import sqrt, sin, cos, exp, arcsin

def calcE0_thin(n, aoi, wn):
    """    return relative Field amplitude at z=0 in the rarer medium for a thin 
    film (d << d_p) as numpy array: [E0x, E0y, E0z] Fringeli, 1993 eqs 8-10: 
    """
    n31 = n[2]/n[0]
    n32 = n[2]/n[1]
    
    denom = sqrt(1-n31**2)*sqrt((sin(aoi)**2)*(1+n31**2)-n31**2)
    
    E0x = (2*cos(aoi)*sqrt((sin(aoi)**2)-n31**2)) / (denom) 
    E0y = (2*cos(aoi))/sqrt(1-n31**2)
    E0z = (2*sin(aoi)*cos(aoi)*(n32**2)) / denom
    return _np.array([E0x,E0y,E0z])

def calcE0_thick(n, aoi, wn):
    """    return relative Field amplitude at z=0 in the rarer medium for a thick 
    film (medium 2 = bulk, medium3 is ignored) as numpy array: [E0x, E0y, E0z] 
    Fringeli, 1993 eqs 5-7: 
    """
    n21 = n[1]/n[0]
    denom = sqrt(1-n21**2)*sqrt((sin(aoi)**2)*(1+n21**2)-n21**2)
    E0x = (2*cos(aoi)*sqrt((sin(aoi)**2)-n21**2)) / (denom) 
    E0y = (2*cos(aoi))/sqrt(1-n21**2)
    E0z = (2*sin(aoi)*cos(aoi)) / denom
    return _np.array([E0x,E0y,E0z])

def calc_dp(n1, n2, wavelength, aoi):
    """ Return the depth of penetration in the unit of the input wavelength
    n1: refractive index of the denser medium
    n2: refractive index of the rarer medium
    wavelength: wavelength in the vacuum
    aoi: angle of incidence in radians"""
    
    lambda1 = wavelength / n1 #wavelength in the denser medium
    n21 = n2/n1
    
    dp = (lambda1) / (2.*pi*sqrt((sin(aoi)**2.)-n21**2.)) 
    return dp

def calc_de(aoi, n1, n2, dp, E_0, d = inf, N = 1):
    """Return the effective thickness in the unit of the input wavelength.
    Accordinng to Fringeli, 1993 eq 13. Thin or thick film approximation is 
    obtained by d = numpy.inf . The approximations for thin films given by 
    Fringeli and Harrick are probably unnecessary.
    N: Number of reflections
    !!! MR: Something seems to be wrong: cannot reproduce Table I in Harrick, 
    1987 p. 49
    I think there is a mistake in that table according to p44 the ratio 
    between the values at 45° (last column of Table I) should be 0.5 which it 
    clearly is not. However it is when using this function see for instance
    piratr_testscript.py!!! 
    TODO: check other tabulated values of d_e
    """
    n21 = n2/n1
    de = (((E_0**2)*n21*dp)/(2*cos(aoi)))*(1-exp((-2*d)/dp))
    return de*N


def calc_thetacrit(n1, n2):
    
    return arcsin((n2/n1))

def calc_S_mol(model, R, alpha=0):
    """Return the  molecular order Parameter S_mol, for model 
    IRE.model, dichroic ration R (A_p/A_s), and  alpha, the angle betwenn 
    molecule axis and transition dipole moment. For alpha = 0 the order 
    parameter of the transition dipole moments S is returned. This equation
    is found in Tamm and Tatulian, 1997"""
    alpharad = pi*(alpha/180)
    S =( (2*((model.E0[0]**2)-R*(model.E0[1]**2)+model.E0[2]**2))
        /( (3*(cos(alpharad))**2-1)  
          *((model.E0[0]**2)-R*(model.E0[1]**2)-2*model.E0[2]**2) )
        )
    return S

class model(object):
    def __init__(self, IRE, E0):
        self.E0 = E0
        self.E0_p = sqrt(E0[0]**2+E0[2]**2)
        self.E0_s = E0[1]

class thin_film(model):
    
    def __init__(self, IRE):
        self.dp = calc_dp(IRE.n[0],IRE.n[2], IRE.wavelength, IRE.aoi)
        E0 = calcE0_thin(IRE.n, IRE.aoi, IRE.wavenumber)
        self.theta_crit = calc_thetacrit(IRE.n[0], IRE.n[2])
        if _np.any(IRE.aoi < self.theta_crit):
            raise ValueError('The chosen angle of incidence is below the' 
                             ' critical angle of total reflection')
        super().__init__(IRE, E0)
        
class thick_film(model):
    """Thick film model, i.e. Thickness(Medium 2) >> d_p, Medium 3 is not 
    considered."""
    def __init__(self, IRE):
        self.dp = calc_dp(IRE.n[0],IRE.n[1], IRE.wavelength, IRE.aoi)
        E0 = calcE0_thick(IRE.n, IRE.aoi, IRE.wavenumber)
        super(thick_film, self).__init__(IRE, E0)
        self.de = calc_de(IRE.aoi, IRE.n[0], IRE.n[1], self.dp, E0)
        self.de_p = calc_de(IRE.aoi, IRE.n[0], IRE.n[1], self.dp, self.E0_p)
        self.de_s = calc_de(IRE.aoi, IRE.n[0], IRE.n[1], self.dp, self.E0_s)
        
        self.theta_crit = calc_thetacrit(IRE.n[0], IRE.n[1])
        if _np.any(IRE.aoi < self.theta_crit):
            raise ValueError('The chosen angle of incidence is below the' 
                            ' critical angle of total reflection')
        
        
        
class IRE(object):
    """ Object representing an Internal reflection element (IRE) at a 
    specific wavelength [nm] with the refractive index n =(n1,n2,n3) and 
    angle of incidence (aoi_degree) in degrees
    n1: refractive index of IRE
    n2: refractive index of thin/thick film
    n3: refractive index of bulk medium (optional), 
        when ommitted only thick film models are calculated
    !!! Wavelengths and thicknesses are in nm !!!
    """
    
    def __init__(self, n, aoi_degree, wavenumber, N = 1):
        self.n = _np.array(n)
        self.wavenumber =  _np.array(wavenumber)
        self.wavelength =  _uc.wavenum2nm(self.wavenumber)
        self.lambda_medium = self.wavelength/self.n
        self.aoi_deg =  _np.array(aoi_degree)
        self.aoi = self.aoi_deg*pi/180

        if len(n) == 3:
            self.thin_film = thin_film(self)
        self.thick_film = thick_film(self)
        
#    def calcEzero(self):
#        return _np.array([0,0,0])
        