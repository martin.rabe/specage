
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 10:51:14 2018
Helpful functions for ASCII readers.
@authors: Martin Rabe, S.Tecklenburg
"""
import numpy as _np

def replaceinfile(infilename, outfilename, replacements):
    """
    Replace strings in ASCII files and save as copy of original file.

    Parameters
    ----------
    infilename : str
        Filepath to input file.
    outfilename : str
        Filepath to output file.
    replacements : dict
        Dictionary mapping the str to be replaced to the str to be replaced 
        with. For Example: {'-':'+'} replaces all occurences of '-' with '+'

    Returns
    -------
    None.

    """
    with open(infilename) as infile, open(outfilename, 'w') as outfile:
        for line in infile:
            
            for src, target in replacements.items():
                line = line.replace(src, target)
            
            outfile.write(line)


def comma_replace(filename):
    """Replace all ',' within a file with '.' and save it as filename.crp"""
    replacements = {',':'.'}
    #foldername, filename =path.split(filename)
    #basename, extension =path.splitext(filename)
    
    with open(filename) as infile, open(filename+'.crp', 'w') as outfile:
        for line in infile:
            
            for src, target in replacements.items():
                line = line.replace(src, target)
            
            outfile.write(line)

def find_line(filename, linestring):
    """Return indeces of all full line appearances of linestring in filename."""
    indeces = []
    with open(filename) as infile:
        for idx, line in enumerate(infile):
            if line == linestring+'\n':
                indeces.append(idx)
                
    return indeces

def find_str_in_line(filename, substr):
    """Return indeces of all appearances of substr in the lines of filename."""
    indeces = []
    with open(filename) as infile:
        for idx, line in enumerate(infile):
            if substr in line:
                indeces.append(idx)
                
    return indeces

def readlines(filename, linenumbers ):
    """Return lines with the linenumbers in the ascii file filename. linenumbers 
    should behave like a list of integers. Hint:
    to get the content of all lines that contain a specific string, use: 
    content = readlines(filename, find_str_in_line(filename, string)) """
    
    outlist = [''] * len(linenumbers)
    with open(filename) as infile:
        for idx, line in enumerate(infile):    
            if idx in linenumbers:
                outlist[linenumbers.index(idx)] = line
    return outlist

def readASCII_file(file):
    """Return all lines of the file in a list"""
    with open(file, encoding="latin-1") as File:
        curFile = File.read().splitlines()
    return curFile

def readASCII_table(file, headlines=0, delim = None):
    """Read simple table from ASCII file with defined number of headerlines and
    delimiter and return data as numpy array and headerlines as list. Hint: 
    delim = None treats consecutive whitespace as single separator and also 
    works with leading or trailing whitespace."""
    with open(file, encoding="latin-1") as File:
        curFile = File.read().splitlines()         
        
    curMat=_np.zeros((len(curFile)-headlines,
                      len(curFile[headlines+1].split(sep=delim))))
    l=len(curMat)

    for i in range(0,l,1):

        tmp = curFile[i+headlines].split(sep=delim)
  
        # replace missing values by NaN:
        for n, string in enumerate(tmp):
            tmp[n] = 'NaN' if string == '' else string
        curMat[i] = tmp
        
    return curMat, curFile[:headlines]