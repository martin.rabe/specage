#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Dec 20th 2021
Peak fitting interface. This file contains an implementation that uses 
scipy.optimize.least_squares. This is not used in the current pkfit 
interface, which uses scipy.optimize.curve_fit. The implementation here gives 
no access to the parameter errors but has some other benefits that might be 
useful for specific cases. Hence it is still here, but not maintained very well.


@author: MR, 2021
"""

import  numpy as np
from  scipy.optimize import least_squares as ls
from numpy import log, exp, pi, sqrt, inf
from specage.spcl import Spectrum as spc
from specage.spcl import SpectralSeries as spcser
from specage.spcl import TimeSeries as tser



def fitpeak(specageobj, initpara, fitparbool, model, 
               xrange=[], lb = -inf, ub=inf, **kwargs): 
    if isinstance(specageobj, spc):
        fitpeakmodelsingle_ls(specageobj, initpara, fitparbool, model, xrange, lb, 
                           ub, **kwargs)
    elif isinstance(specageobj, spcser) or isinstance(specageobj, tser):
        fitpeakmodelseries(specageobj, initpara, fitparbool, model, xrange, lb, ub, 
                           **kwargs)
    else:
        raise(TypeError('specageobj must be Spectrum, TimeSeries, or SpectralSeries'))
                        




def fitpeakmodelseries(series, initpara, fitparbool, model, 
                       xrange, lb, ub, **kwargs):
    for spectrum in series.get_spectra():
        fitpeakmodelsingle_ls(spectrum, initpara, fitparbool, model, xrange, lb, 
                           ub, **kwargs)
        

    
def fitpeakmodelsingle_ls(spectrum, initpara, fitparbool, model, 
                       xrange, lb, ub, **kwargs):    
    if xrange != []:
        spectrum.set_peaklimit(xrange)

    fitparastart = initpara[fitparbool]
    if hasattr(lb, '__len__') and hasattr(ub, '__len__'):
        try:
            lb = lb[fitparbool]
            ub = ub[fitparbool]
        except IndexError():
            raise
            
    fixpara=initpara[~fitparbool]
    # there maybe problems if the type is complex numbers thus first
    if np.isreal(spectrum.get_peak()).all():
         peak = np.real(spectrum.get_peak())
    else:
        raise TypeError('The Peak contains complex values. Fitting is not feasible')
    
    fitresult = ls(fixparaobjectivefun, 
                   fitparastart, 
                   bounds = (lb, ub),
                   args=(peak[:,0], 
                         peak[:,1], 
                         fitparbool, 
                         fixpara,
                         model), 
                         **kwargs)
    
    
    #now create new spectra from the results:
    peaks =  evalfitfuns_fp(fitresult.x, 
                           spectrum.get_peak()[:,0], 
                           fitparbool, 
                           initpara[~fitparbool], 
                           model)
    peaklist=[]
    for intensity in peaks.T:
        peaklist.append(spc(spectrum.get_peak()[:,0], intensity))
    
    PeakSeries =spcser(peaklist,np.arange(peaks.shape[1]),cName = 'number')
    
    
    # the fitresults returned comprise also fixed parameters
    fitresult.parameters = np.copy(initpara)
    fitresult.parameters[fitparbool] = fitresult.x
    spectrum.set_peakfitresults(PeakSeries, fitresult)
    


# this is a function decorator that allows to store the required length 
# of the parameter vector as attribute of the function object:
def setparameternum(paramslength):
    def wrapper(func):
        func.parlength=paramslength
        return func
    return wrapper

    
# the function definition is preceeded by a call of the decorator that defines 
# the length of the params array:
@setparameternum(2)
def lin(params, x):
    '''Returns values of linear function with 2 parameters: 
        params[0]+params[1]*x'''
    return params[0]+params[1]*x


@setparameternum(3)
def gaussamp(params, x):
    '''Return value of gaussian at x, using 3 parameters. x may be numpy.array.
    params =[h, xpos, w], with:
        h: height 
        xpos: position
        w: width
    Note: The area under the curve can be calculated by: 
        A= h*sqrt(2*pi*(w**2))'''
    h = params[0]
    xpos = params[1]
    w = params[2]
    
    f = h*exp(-np.power((x-xpos),2)/(2*np.power(w,2)))
    return f

@setparameternum(3)
def gaussarea(params, x):
    ''' Returns values of gaussian at x, using 3 parameters 
    params=[A, xpos, fwhm]:
        A: peak area 
        xpos: position
        fwhm: full width at half maximum.
        '''
    A = params[0]
    xpos = params[1]
    fwhm = params[2]
    f = (A * exp(((-4)*log(2)*np.power((x-xpos),2))/np.power(fwhm,2)) /
        (fwhm*sqrt(pi/(4*log(2)))))
    return f

@setparameternum(3)
def lorentzamp(params, x):
    ''' Returns values of lorentzian at x, using 3 parameters 
    params=[h, xpos, fwhm]:
        h: height, amplitude 
        xpos: position
        fwhm: full width at half maximum.
        '''
    h = params[0]
    xpos = params[1]
    fwhm = params[2]
    f = h/(1+4*np.power(((x-xpos)/fwhm),2))
    return f

@setparameternum(3)
def lorentzarea(params, x):
    ''' Returns values of lorentzian at x, using 3 parameters 
    params=[A, xpos, fwhm]:
        A: peak area
        xpos: position
        fwhm: full width at half maximum.
        '''
    A = params[0]
    xpos = params[1]
    fwhm = params[2]
    f = ((2*A/(pi*fwhm)) *
        1/(1+4*np.power(((x-xpos)/fwhm),2)))
    return f


def evalfitfuns(params, x, funlist):
    indx = 0
    result=np.zeros((len(x), len(funlist)))
    for funnr, fun in enumerate(funlist):
        result[:,funnr] = fun(params[indx:indx+fun.parlength],x)
        indx += fun.parlength
    return result

def objectivefun(params, x, y, funlist):
    '''returns the residuals so that it can be used as input for ls method'''
    return evalfitfuns(params, x, funlist).sum(1)-y

def fixparaobjectivefun(fitparams, x, y, fitparbool, fixedparameters, funlist):
    '''some doc'''
    # construct input for objectivefun
    params=np.zeros(fitparbool.shape)
    params[fitparbool] = fitparams
    params[~fitparbool] = fixedparameters
    
    result = objectivefun(params, x, y, funlist)
    return result

def evalfitfuns_fp(fitparams, x, fitparbool, fixedparameters, funlist):
    '''some doc'''
    # construct input for objectivefun
    params=np.zeros(fitparbool.shape)
    params[fitparbool] = fitparams
    params[~fitparbool] = fixedparameters
    return evalfitfuns(params, x, funlist)
    
    
#%% here follows some code for testing
if __name__=='__main__':  
    import specage.ir_readers as ir
    import matplotlib.pyplot as plt
    File = 'C://Zmirror//smartCC//Bruker IR//18_07_10//18_07_10_MR51b.0001.dpt'
    sp=ir.bruk_dpt(File)
    lim=[1620, 1690]
    sp.set_peaklimit(lim)
    
    fitparbool = np.bool8([ 1,1,
                           # 1,1,1,
                           # 1,1,1,
                           # 1,1,1,
                           1,1,1,
                           ])
    
    fp = [1620, -0.00622]# fixing point for initial guess baseline [x1,y1]
    m= 0.5E-6
    n= fp[1]-m*fp[0]
    initpara = np.array([n, m,
                         # 0.1E-3, 1630, 3,
                         # 0.5E-3, 1642, 5,
                         2.7E-3, 1660, 5,
                         # 1E-3, 1675, 5,
                         ])
    lb = np.array([-1,   -1,
                    # 0, 1610, 2, 
                    # 0, 1630.0, 2,
                    0, 1655, 2,
                    # 0, 1670, 2,
                    ])
    ub = np.array([      1,    1,
                    # np.inf, 1640, 25,   
                    # np.inf, 1650, 25,
                    np.inf, 1665, 25,
                    # np.inf, 1680, 25,
                    ])
    
    model = [lin, 
             gaussamp, 
             # gaussamp, 
             # gaussamp,
             # gaussarea,
             ]
             
    
    initialguess = evalfitfuns(initpara, sp.get_peak()[:,0], model)
    
    plt.close('all')
    ax1 = plt.axes(title='Initial guess')
    ax1.plot(sp.get_peak()[:,0], sp.get_peak()[:,1],'o')
    #plot initialguess baseline:
    ax1.plot(sp.get_peak()[:,0], initialguess[:,0])
    #plot functions on baseline
    ax1.plot(sp.get_peak()[:,0], (initialguess[:,1:].T+initialguess[:,0]).T)
             
    #plot sum of peaks on baseline:
    ax1.plot(sp.get_peak()[:,0], initialguess[:,1:].sum(1)+initialguess[:,0])

    
    fixedparameters=initpara[~fitparbool]
    fitparameters=initpara[fitparbool]

    
    fitpeak(sp, initpara, fitparbool, model, lb=lb, ub=ub, verbose =2, max_nfev=300)
    
    plt.figure()
    ax2 = plt.axes(title='Result')
    ax2.plot(sp.get_peak()[:,0], sp.get_peak()[:,1],'o')
    #baseline 
    ax2.plot(sp.fittedpeaks.x, sp.fittedpeaks.intarray[:,0])
        #plot functions on baseline
    ax2.plot(sp.get_peak()[:,0], 
             (sp.fittedpeaks.intarray[:,1:].T+sp.fittedpeaks.intarray[:,0]).T)
             
    #plot sum of peaks on baseline:
    ax2.plot(sp.get_peak()[:,0], sp.fittedpeaks.intarray.sum(1))

    
    