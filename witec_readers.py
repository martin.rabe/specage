# -*- coding: utf-8 -*-
"""
Readers for ASCII files exported from Witec 'Project Five' Software.
"""
#M.Rabe 2018

import os

import numpy as _np

from specage.spcl import Spectrum
from specage.ascii_magic import readASCII_table

def multi_witASCII(filelist):
    """Read multiple ASCII files created by Witecs 'Project Five' software and 
    return list of Spectrum objects"""
    outputlist=[]
    for file in filelist:
        outputlist.append(witASCII(file))
    return outputlist
    
def witASCII(file):
    """Read single ASCII file created by Witecs 'Project Five' software and 
    return a Spectrum object"""
    curMat, header = readASCII_table(file, headlines=2, delim = '\t')
    spec = Spectrum(curMat[:,0], curMat[:,1], 
                             xlabel='\lambda / nm', ylabel='Int', #MR: is that correct?
                             FileName=os.path.split(file)[1])

    return spec
