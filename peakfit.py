#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 11:08:34 2018
rewrote to generalized form: Nov 24, 2021

Deprecated interface for peak fitting. Provided for backward comptibility only.
For new projects use specage.pkfit !!!


@author: MR
"""

import  numpy as np
from  scipy.optimize import least_squares as ls
from numpy import log, exp, pi, sqrt, inf
from scipy.linalg import svd
from specage.spcl import Spectrum as spc
from specage.spcl import SpectralSeries as spcser
from specage.spcl import TimeSeries as tser

'''first we define some peak functions:'''
def gaussianarea(x, A, xpos, fwhm):
    '''another form of the gaussian using the area and fwhm as parameters'''
    f = (A * exp(((-4)*log(2)*np.power((x-xpos),2))/np.power(fwhm,2)) /
        (fwhm*sqrt(pi/(4*log(2)))))
    return f

def gaussianamp(x, h, xpos, w):
    '''Return value of gaussian. x may be numpy.array. h: height, 
    xpos: position, w: width. Hint: The area under the curve can be calculated
    by: A= h*sqrt(2*pi*(w**2))'''
    f = h*exp(-np.power((x-xpos),2)/(2*np.power(w,2)))
    return f

'''Here follow the functions necessary to structure the peak functions to a 
sum of peaks and to enable usage of least square fitting function 
(scipy.optimize.least_squares())'''

def addpeaks(x, size_lst, xpos_lst, w_lst, peakfunc):
    '''
    Return Value of sum  of peaks whose shapes are defined by peakfunc. 
    The peaks are defined by the lists size (can be height or area), xpos, w
    which all must have the same length and whos length determines the number 
    of peaks.
    '''
    gaussians = np.zeros([len(x), len(size_lst)])
    sums = np.zeros([len(x),1])
    for ind, h in enumerate(size_lst):
        xpos = xpos_lst[ind]
        w = w_lst[ind]
        gaussians[:,ind] = peakfunc(x, h, xpos, w)
    
    sums[:,0] = np.sum(gaussians, axis=1)        
    return sums, gaussians

def peak_sumres(params, x, y, fitparbool, fixedparameters, peakfunc):
    '''Returns residuals of sum of peaks with respect to y at x. Function 
    is designed to work as input for scipy.optimize.least_squares(). Allows 
    specific parameters to be set as fixed. 
    All arguments must be numpy.array.
    fitparbool: [h1, xpos1, w1, h2, ...] With the elements 
    being Boolean to indicate if the parameter is a fit parameter.
    params: np.array of the form [fitparbool]
    fixedparameters: np.array of the form [~fitparbool]'''
    
    peakSum = peak_sum(params, x, fitparbool, fixedparameters, peakfunc)[0]
    return peakSum[:,0]-y



def peak_sum(params, x, fitparbool, fixedparameters, peakfunc):
    '''Returns sum of peaks and single peaks at x. Function 
    is designed to work as input for scipy.optimize.least_squares(). Allows 
    specific parameters to be set as fixed. 
    All arguments must be numpy.array.
    fitparbool: [h1, xpos1, w1, h2, ...] With the elements 
    being Boolean to indicate if the parameter is a fit parameter.
    params: np.array of the form [fitparbool]
    fixedparameters: np.array of the form [~fitparbool]'''
    
    fitparbool=fitparbool.reshape([int(len(fitparbool)/3),3])
    inputarray = np.zeros(fitparbool.shape)
    inputarray[fitparbool] = params
    inputarray[~fitparbool] = fixedparameters
    
    sums, peaks = addpeaks(x, inputarray[:,0], 
                              inputarray[:,1], 
                              inputarray[:,2],
                              peakfunc)
    return sums, peaks

def fitpeak(specageobj, initpara, fitparbool, peakfunc, 
               xrange=[], lb = 0, ub=inf, verbose = 2): 
    if isinstance(specageobj, spc):
        fitpeaksingle(specageobj, initpara, fitparbool, peakfunc, xrange, lb, 
                      ub, verbose)
    elif isinstance(specageobj, spcser) or isinstance(specageobj, tser):
        fitpeakseries(specageobj, initpara, fitparbool, peakfunc, xrange, lb, ub, 
                      verbose)
    else:
        raise(TypeError('specageobj must be Spectrum, TimeSeries, or SpectralSeries'))
                        




def fitpeakseries(series, initpara, fitparbool, peakfunc, 
               xrange, lb, ub, verbose):
    for spectrum in series.get_spectra():
        fitpeaksingle(spectrum, initpara, fitparbool, peakfunc, xrange, lb, 
                      ub, verbose)
        

    
def fitpeaksingle(spectrum, initpara, fitparbool, peakfunc, 
               xrange, lb, ub, verbose):    
    if xrange != []:
        spectrum.set_peaklimit(xrange)

    fitparastart = initpara[fitparbool]
    if hasattr(lb, '__len__') and hasattr(lb, '__len__'):
        try:
            lb = lb[fitparbool]
            ub = ub[fitparbool]
        except IndexError():
            raise
            
    fixpara=initpara[~fitparbool]
    # there maybe problems if the type is complex numbers thus first
    if np.isreal(spectrum.get_peak()).all():
         peak = np.real(spectrum.get_peak())
    else:
        raise TypeError('The Peak contains complex values. Fitting is not feasible')
    
    fitresult = ls(peak_sumres, 
                   fitparastart, 
                   bounds = (lb, ub),
                   args=(peak[:,0],
                         peak[:,1]-peak[:,2], 
                         fitparbool, 
                         fixpara,
                         peakfunc), 
                   verbose=verbose)
    
    
    #now create new spectra from the results:
    sums, peaks = peak_sum(fitresult.x, 
                           spectrum.get_peak()[:,0], 
                           fitparbool, 
                           initpara[~fitparbool], 
                           peakfunc)
    peaklist=[]
    for intensity in peaks.T:
        peaklist.append(spc(spectrum.get_peak()[:,0], intensity))
    
    PeakSeries =spcser(peaklist,np.arange(peaks.shape[1]),cName = 'number')

    
    
    fitresult.parameters = np.copy(initpara)
    fitresult.parameters[fitparbool] = fitresult.x
    spectrum.set_peakfitresults(PeakSeries, fitresult)
    
    

if __name__=='__main__':  # here follows some code for testing
    import specage.ir_readers as ir
    import matplotlib.pyplot as plt
    File = 'Z://smartCC//Bruker IR//18_07_10//18_07_10_MR51b.0001.dpt'
    sp=ir.bruk_dpt(File)
    lim=[1620, 1690]
    sp.set_peaklimit(lim)
    
    fitparbool = np.array([True,True,True,
                           True,True,True,
                           True,True,True,
                           ])
    initpara = np.array([
                         1E-1, 1638, 10,
                         1E-1, 1660, 10,
                         1E-1, 1675, 10,
                         ])
    lb = np.array([
                    0, 1630, 5,
                    0, 1655, 5,
                    0, 1670, 5,
                    ])
    ub = np.array([
                    np.inf, 1645, 25,
                    np.inf, 1665, 25,
                    np.inf, 1680, 25,
                    ])
    
    fitresults, pcov = fitpeaksum(sp, initpara, fitparbool, gaussianamp, 
                                  lb=lb, ub=ub, )

    
    plt.close('all')
    ax1 = plt.axes()
    ax1.plot(sp.get_peak()[:,0], sp.get_peak()[:,1]-sp.get_peak()[:,2],'o')

    sums, gaussians = peak_sum(fitresults.x, sp.get_peak()[:,0], 
                               fitparbool, initpara[~fitparbool], gaussianamp)
    ax1.plot(sp.get_peak()[:,0], sums[:,0])
    ax1.plot(sp.get_peak()[:,0], gaussians )
    with np.printoptions(precision=5, suppress = True):
        print(fitresults.x.reshape((3,3)))
    