#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example script that shows how to use the peakfit module of specage.
The data used are spectra recorded by OceanOptics HR2000+ spectrometer.
matplotlib and numpy are needed. 
"""

"""import the necessary modules:"""
import matplotlib.pyplot as plt
import numpy as np
"""The following specage modules are neede:"""
from specage.ov_readers import spectra_time_series as read_ts
from specage.spcl import TimeSeries as tseries
import specage.pkfit as pkfit

###############         read & structure the data       ######################

Filepath = './/example_data//'
Filename = 'examplesTS.txt'

"""read ASCII data by using the reader for OceanOptics Spectra and create 
the TimeSeries Object 'ts' """
ts = read_ts(Filepath+Filename) 

"""ts contains all spectra and their recording times as Spectrum objects:"""
AniceSpectrum = ts[30] 

###############         inspect the data       ######################

"""a single spectrum can be be plotted by:"""
plt.figure();
plt.plot(AniceSpectrum.x, AniceSpectrum.intensity);


"""all spectra can be plotted by means of ts:"""
plt.figure();
plt.plot(ts.x, ts.intarray);


###############         peak fitting       ######################

"""First, some input parameters mus be defined, 
starting with the model which is defined by a list of functions that are 
provided in pkfit. The model can contain any number of individual functions:"""
model = [pkfit.lin,             # a linear baseline
         pkfit.gaussarea,       # a gaussian peak with the area as parameter
         pkfit.lorentzarea,     # a lorentzian peak with the area as parameter
         ]

"""Also the x-range must be defined:"""
xrange = np.array([503,680])

""" The linear baseline function pkfit.lin has 2 parameters: 
       - y-intercept 
       - slope
    The peakfunctions have 3 parameters: 
       - size (peak height(amplitude) or area),
       - x-position, 
       - width.
    These informations are important for the model creation and can be found 
    in the doc of each function. --> help(function)"""

"""For fitting, initial starting values must be defined for all parameters in a 
numpy array of length [n,] and "dtype=object", with n being the sum of the number of all 
parameters. For example for the model defined above n = 8. Fitting parameters 
are defined as special variables of the pkfit.singlepar class whose value is 
the initial starting value. They contain also their lower and upper bounds the 
respective keyword arguments "lb" and "ub" default to -inf and inf. 
Fixed parameters are defined by any numeric type:"""

n = pkfit.singlepar(0, lb=-50, ub=500)   # this is a fit parameter
m = 0                                    # this parameter is fixed

a1 = pkfit.singlepar(1000, lb=0)
x1 = pkfit.singlepar(525, lb=500, ub=550)
w1 = pkfit.singlepar(25, lb=5, ub=100)

a2 = pkfit.singlepar(3000, lb=0)
x2 = pkfit.singlepar(560, lb =550, ub=650)
w2 = pkfit.singlepar(25, lb=5, ub=100)

initpara = np.array([ 
                     n ,   m,       # y-intercept, slope,     --> pkfit.lin
                     a1, x1, w1,    # area1, x-pos1, width1,  --> pkfit.gaussarea
                     a2, x2, w2,    # area2, x-pos2, width2,  --> pkfit.lorentzarea
                     ], dtype=object)
    

"""Now, the fitpeak function is called to fit the peak and all the 
parameters defined above are passed. The console output can be controlled by 
the 'verbose' keyword argument. verbose = 2 shows all progress and indicates 
the success of the fitting routine."""

res, err =pkfit.fitpeak(AniceSpectrum, 
                        initpara, 
                        model, 
                        xrange = xrange,
                        fitblcorrected = False,
                        verbose=2,                
                        )


##################        plotting of the results    #########################
plt.close('all')
ax1 = plt.axes()


# plot the data
ax1.plot(AniceSpectrum.get_peak()[:,0], 
         AniceSpectrum.get_peak()[:,1],'o')

# plot the fitted baseline
ax1.plot(AniceSpectrum.get_peak()[:,0], AniceSpectrum.fittedpeaks.intarray[:,0])

# plot the fitted peaks on the baseline
ax1.plot(AniceSpectrum.get_peak()[:,0], 
         (AniceSpectrum.fittedpeaks.intarray[:,1:].T
         +AniceSpectrum.fittedpeaks.intarray[:,0]).T)

# plot the sum of peaks+baseline
ax1.plot(AniceSpectrum.get_peak()[:,0], 
         np.sum(AniceSpectrum.fittedpeaks.intarray, axis=1), 
         color='y')

# print the fit parameters
with np.printoptions(precision=5, suppress = True):
    print(AniceSpectrum.fitresults.parameters)



##################        fit the whole series    #########################
"""With the peakfit module it is possible to fit complete Series (TimeSeries 
or SpectralSeries objects). Here this is shown with the TimeSeries ts:"""

res, er = pkfit.fitpeak(ts, 
                        initpara, 
                        model, 
                        xrange = xrange,
                        verbose=1,
                        )


"""The specific results can now be plotted for instance the areas, positions 
and widths of the peaks: """

fittedpeakareas=np.array([spectr.fitresults.parameters[[2,5]] 
                            for spectr in ts])

fittedpeakpositions=np.array([spectr.fitresults.parameters[[3,6]] 
                            for spectr in ts])

fittedpeakwidths=np.array([spectr.fitresults.parameters[[4,7]] 
                            for spectr in ts])

plt.figure()
plt.plot(ts.t_rel/60, fittedpeakareas,'o')
plt.ylabel('peak area')
plt.xlabel('time [min]');

plt.figure()
plt.plot(ts.t_rel/60, fittedpeakpositions,'o')
plt.ylabel('peak position')
plt.xlabel('time [min]');

plt.figure()
plt.plot(ts.t_rel/60, fittedpeakwidths,'o')
plt.ylabel('peak width')
plt.xlabel('time [min]');


################ global fit to subgroup of the series   ###################
"""Now we want to apply a global fit. which means that specific parameters are 
shared amongst the datasets and/or within one dataset. This calculation can be 
time consuming for large datasets hence a subset is used here:"""

ts2 = tseries(ts[21:34])

""" Now, The shared parameters are redefined:"""

pkfit.singlepar

n = pkfit.sharedpar(0, lb=-50, ub=500)

x1 = pkfit.sharedpar(525, lb=500, ub=550)

x2 = pkfit.sharedpar(560, lb =550, ub=650)

w1 = pkfit.sharedpar(25, lb=5, ub=100)

w2 = w1

"""As the other parameters stay single (i.e. unshared) the initial parameters 
are:"""

initpara = np.array([ 
                     n ,   m,       # y-intercept, slope,     
                     a1, x1, w1,    # area1, x-pos1, width1,  
                     a2, x2, w1,    # area2, x-pos2, width2 = width1
                     ], dtype=object)

"""note: if parameters are supposed to be shared within one spectrum it is 
important that they are identical in initpara. I.e. "initpara[m] is 
initpara[n]" returns True, as in:"""

initpara[4]==initpara[7]

"""For global fitting, the fitpeak function is called with the keyword argument 
"fitglobal=True" """

res, er = pkfit.fitpeak(ts, 
                        initpara, 
                        model, 
                        fitglobal=True,
                        xrange = xrange,
                        verbose=2,
                        ftol=1E-8)

"""Plot some of the results """

fittedpeakareas=np.array([spectr.fitresults.parameters[[2,5]] 
                            for spectr in ts2])

fittedpeakpositions=np.array([spectr.fitresults.parameters[[3,6]] 
                            for spectr in ts2])

fittedpeakwidths=np.array([spectr.fitresults.parameters[[4,7]] 
                            for spectr in ts2])

plt.figure()
plt.plot(ts2.t_rel/60, fittedpeakareas,'o')
plt.ylabel('peak area')
plt.xlabel('time [min]');

plt.figure()
plt.plot(ts2.t_rel/60, fittedpeakpositions,'o')
plt.ylabel('peak position')
plt.xlabel('time [min]');

plt.figure()
plt.plot(ts2.t_rel/60, fittedpeakwidths,'o')
plt.ylabel('peak width')
plt.xlabel('time [min]');