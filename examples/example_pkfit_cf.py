#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example script that shows how to use the peakfit module of specage.
The data used are spectra recorded by OceanOptics HR2000+ spectrometer.
matplotlib and numpy are needed
"""

"""import the necessary modules:"""
import matplotlib.pyplot as plt
import numpy as np
"""The following specage modules are neede:"""
from specage.ov_readers import spectra_time_series as read_ts
import specage.pkfit_cf as pkfit

###############         read & structure the data       ######################

Filepath = './/example_data//'
Filename = 'examplesTS.txt'

"""read ASCII data by using the reader for OceanOptics Spectra and create 
the TimeSeries Object 'ts' """
ts = read_ts(Filepath+Filename) 

"""ts contains all spectra and their recording times as Spectrum objects:"""
AniceSpectrum = ts[30] 

###############         inspect the data       ######################

"""a single spectrum can be be plotted by:"""
plt.figure();
plt.plot(AniceSpectrum.x, AniceSpectrum.intensity);


"""all spectra can be plotted by means of ts:"""
plt.figure();
plt.plot(ts.x, ts.intarray);







###############         peak fitting       ######################

"""First, some input parameters mus be defined, 
starting with the model which is defined by a list of functions provided in 
pkfit. The model can contain any number of individual functions:"""
model = [pkfit.lin,             # a linear baseline
         pkfit.gaussarea,       # a gaussian peak with the area as parameter
         pkfit.lorentzarea,     # a lorentzian peak with the area as parameter
         ]

"""Also the x-range must be defined:"""
xrange = np.array([503,680])

""" The linear baseline function pkfit.lin has 2 parameters: 
       - y-intercept 
       - slope
    The peakfunctions have 3 parameters: 
       - size (peak height(amplitude) or area),
       - x-position, 
       - width.
    These informations are important for the model creation and can be found 
    in the doc of each function. --> help(function)
_______________________________________________________________________________

For fitting initial starting values must be defined for all parameters in a 
numpy array of of length [n,], with n being the sum of the number of all 
parameters. For example for the model defined above n = 8:"""

initpara = np.array([ 
                     0.0 ,   0,        # y-intercept, slope,       --> pkfit.lin
                     1500, 530, 10,    # height1, x-pos1, width1,  --> pkfit.gaussarea
                     500 , 565, 10,    # height2, x-pos2, width2,  --> pkfit.lorentzarea
                     ])



"""Next, it is defined which parameters are fitting parameters (True) or 
fixed (False) during fitting. The order and shape of the numpy array is the 
same as for starting values."""
fitparbool = np.bool_([1, 0,   # y-intercept, slope,
                       1,1,1,  # height1, x-pos1, width1,
                       1,1,1,  # height2, x-pos2, width2,
                       ])



"""Finally, lower (lb) and upper boundary (ub) are defined in the same way. 
If no boundaries are wanted, use np.inf or -np.inf.

NB: Even if one ore more specific parameters are fixed (and hence will be 
ignored by the pkfit module) a value for the boundary must be defined, 
to keep the structure and to avoid confusions."""

lowerbound = np.array([ 
                        -np.inf, -np.inf,   # y-intercept, slope,
                        0.0, 500, 5,        # height1, x-pos1, width1,
                        0, 540, 5,          # height2, x-pos2, width2,
                        ])
upperbound = np.array([
                        0.0, np.inf,          # y-intercept, slope,
                        np.inf, 550, 100,   # height1, x-pos1, width1,
                        np.inf, 590, 100,   # height2, x-pos2, width2,
                        ])

"""Now, the fitpeak function is called to fit the peak and all the 
parameters defined above are passed. The console output can be controlled by 
the 'verbose' keyword argument. verbose = 2 shows all progress and indicates 
the success of the fitting routine."""

pkfit.fitpeak(AniceSpectrum, 
                initpara, 
                fitparbool, 
                model, 
                xrange = xrange,
                lb=lowerbound, 
                ub=upperbound,
                verbose=2,
                )


##################        plotting of the results    #########################
plt.close('all')
ax1 = plt.axes()


# plot the data
ax1.plot(AniceSpectrum.get_peak()[:,0], 
         AniceSpectrum.get_peak()[:,1],'o')

# plot the fitted baseline
ax1.plot(AniceSpectrum.get_peak()[:,0], AniceSpectrum.fittedpeaks.intarray[:,0])

# plot the fitted peaks on the baseline
ax1.plot(AniceSpectrum.get_peak()[:,0], 
         (AniceSpectrum.fittedpeaks.intarray[:,1:].T
         +AniceSpectrum.fittedpeaks.intarray[:,0]).T)

# plot the sum of peaks+baseline
ax1.plot(AniceSpectrum.get_peak()[:,0], 
         np.sum(AniceSpectrum.fittedpeaks.intarray, axis=1), 
         color='y')

# print the fit parameters
with np.printoptions(precision=5, suppress = True):
    print(AniceSpectrum.fitresults.parameters)



##################        fit the whole series    #########################
"""With the peakfit module it is possible to fit complete Series (TimeSeries 
or SpectralSeries objects). Here this is shown with the TimeSeries ts:"""

pkfit.fitpeak(ts, 
              initpara, 
              fitparbool, 
              model, 
              xrange = xrange,
              lb=lowerbound, 
              ub=upperbound,
              verbose=0)


"""The specific results can now be plotted for instance the areas, positions 
and widths of the peaks: """

fittedpeakareas=np.array([spectr.fitresults.parameters[[2,5]] 
                            for spectr in ts])

fittedpeakpositions=np.array([spectr.fitresults.parameters[[3,6]] 
                            for spectr in ts])

fittedpeakwidths=np.array([spectr.fitresults.parameters[[4,7]] 
                            for spectr in ts])

plt.figure()
plt.plot(ts.t_rel/60, fittedpeakareas,'o')
plt.ylabel('peak area')
plt.xlabel('time [min]');

plt.figure()
plt.plot(ts.t_rel/60, fittedpeakpositions,'o')
plt.ylabel('peak position')
plt.xlabel('time [min]');

plt.figure()
plt.plot(ts.t_rel/60, fittedpeakwidths,'o')
plt.ylabel('peak width')
plt.xlabel('time [min]');