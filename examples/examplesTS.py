#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example script that shows some functionalities of the TimeSeries Class.
The data used are spectra recorded by OceanOptics HR2000+ spectrometer.
matplotlib is needed
"""

"""import the necessary modules:"""
import matplotlib.pyplot as plt
from specage.ov_readers import spectra_time_series as read_ts


Filepath = './/example_data//'
Filename = 'examplesTS.txt'

"""read ASCII data by using the reader for OceanOptics Spectra and create 
the TimeSeries Object 'ts' """
ts = read_ts(Filepath+Filename) 



"""ts contains all spectra and their recording times as Spectrum objects:"""
FirstSpectrum = ts[0]




"""a single specturm can be be plotted by:"""
plt.figure();
plt.plot(FirstSpectrum.x, FirstSpectrum.intensity);




"""all spectra can be plotted by means of ts:"""
plt.figure();
plt.plot(ts.x, ts.intarray);




"""the integrals can be calculated by providing the x-limits, 
here they are plotted against the elapsed time:""" 
plt.figure();
plt.plot(ts.t_rel/60, ts.integrals([520, 600]), 'o');





"""intensity at a certain x position can be read:"""
plt.figure();
plt.plot(ts.t_rel/60, ts.intensities_at(505), 'o');
plt.plot(ts.t_rel/60, ts.intensities_at(540), 'o');





"""the peak maximum within a specific x-range is plotted against elapsed time:"""
plt.figure();
plt.plot(ts.t_rel/60, ts.max_positions([490, 600]), 'o');





