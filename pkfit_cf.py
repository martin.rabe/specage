#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Dec 21st 2021
Peak fitting interface. Early implementation that should not be used anymore. 
scipy.optimize.curve_fit is used and individual models 
can be created. linear baselines, lorentzian and gaussian peak shapes are 
yet implemented. Further function can be easily implemented if needed.

Using curve_fit however also has some disadvantages. For instance not all 
information about the optimization is returned. Also it uses different 
functions depending on which method is used (lm or trf) and hence sometimes 
one might obtain a keyword argument error for instance when 'verbose' is used 
without bounds. There is an implementation that uses the 
scipy.optimize.least_squares function implemented in pkfit_ls which might be 
useful if these issue are problematic.

@author: MR, 2021
"""

import  numpy as np
from scipy.optimize import curve_fit as cf
from numpy import log, exp, pi, sqrt, inf
from specage.spcl import Spectrum as spc
from specage.spcl import SpectralSeries as spcser
from specage.spcl import TimeSeries as tser
from types import SimpleNamespace as sns


def fitpeak(specageobj, initpara, fitparbool, model, 
               xrange=[], lb = -inf, ub=inf, fitblcorrected = False, **kwargs): 
    if isinstance(specageobj, spc):
        fitpeakmodelsingle(specageobj, initpara, fitparbool, model, xrange, lb, 
                           ub, fitblcorrected, **kwargs)
    
    elif isinstance(specageobj, spcser) or isinstance(specageobj, tser):
        fitpeakmodelseries(specageobj, initpara, fitparbool, model, xrange, lb, 
                           ub, fitblcorrected, **kwargs)
    
    else:
        raise(TypeError('specageobj must be Spectrum, TimeSeries, or SpectralSeries'))
                        

def fitpeakmodelseries(series, initpara, fitparbool, model, 
                       xrange, lb, ub, fitblcorrected, **kwargs):
    for spectrum in series:
        fitpeakmodelsingle(spectrum, initpara, fitparbool, model, xrange, lb, 
                           ub, fitblcorrected, **kwargs)
   

    
def fitpeakmodelsingle(spectrum, initpara, fitparbool, model, 
                       xrange, lb, ub, fitblcorrected, **kwargs):    
    
    # it maybe that peaklimits are already set in the spectrum and no xrange
    # is provided in the function call here: 
    if xrange != []:
        spectrum.set_peaklimit(xrange)
    else:
        try:
            spectrum.get_peak()
        except AttributeError:
            raise Exception('No x-range provided, neither in the spectrum nor in the function call')
            

    if hasattr(lb, '__len__') and hasattr(ub, '__len__'):
        try:
            lbparam = lb[fitparbool]
            ubparam = ub[fitparbool]
        except IndexError():
            raise
        
    else:
        lbparam = lb
        ubparam = ub
            
    # there maybe problems if the type is complex numbers thus first
    if np.isreal(spectrum.get_peak()).all():
         peak = np.real(spectrum.get_peak())
    else:
        raise TypeError('The Peak contains complex values. Fitting is not feasible')
    
    # construct the objective function
    
    if fitblcorrected:
        y = peak[:,1]-peak[:,2]
    else:
        y = peak[:,1]
    
    fixedparam = initpara[~fitparbool]
    fitstartparam = initpara[fitparbool]

    
    objectivefun=returnobjectivefun(model, fitparbool, fixedparam)
    fitresult = sns() 
    #now fit with curve_fit
    
    result, fitresult.pcov = cf(objectivefun, 
                                peak[:,0], 
                                y,
                                p0=fitstartparam,
                                bounds = (lbparam, ubparam),
                                **kwargs)
    
    # the fitresults returned comprise also the fixed parameters
    fitresult.parameters = np.copy(initpara)
    fitresult.parameters[fitparbool] = result
    fitresult.errors = np.zeros(initpara.shape)
    fitresult.errors[:] = np.nan
    fitresult.errors[fitparbool] = np.sqrt(np.diag(fitresult.pcov))
    
    # it may be useful to have the fit details saved
    fitresult.input=sns()
    fitresult.input.lb = lb
    fitresult.input.ub = ub
    fitresult.input.model = model
    fitresult.input.initpara = initpara
    fitresult.input.fitparbool = fitparbool
    fitresult.input.kwargs = kwargs
    fitresult.input.fitblcorrected =fitblcorrected
    
    # create new specage.SpectralSeries from the results:
    peaks =  evalfitfuns(fitresult.parameters, 
                         spectrum.get_peak()[:,0],  
                         model)
    peaklist=[]
    for intensity in peaks.T:
        peaklist.append(spc(spectrum.get_peak()[:,0], intensity))
    
    PeakSeries =spcser(peaklist,np.arange(peaks.shape[1]),cName = 'number')


    spectrum.set_peakfitresults(PeakSeries, fitresult)


def evalfitfuns(params, x, funlist):
    indx = 0
    result=np.zeros((len(x), len(funlist)))
    for funnr, fun in enumerate(funlist):
        result[:,funnr] = fun(params[indx:indx+fun.parlength],x)
        indx += fun.parlength
    return result

def returnobjectivefun(model, fitparbool, fixedparameters):
    '''returns the objective function'''
    def evalfitfuns_fp(x, *fitparams):
        '''some doc'''
        # construct input for objectivefun
        params=np.zeros(fitparbool.shape)
        params[fitparbool] = fitparams
        params[~fitparbool] = fixedparameters
        return evalfitfuns(params, x, model).sum(1)
    
    return evalfitfuns_fp


def setparameternum(paramslength):
    '''Function decorator that allows to store the required length 
    of the parameter vector as attribute of the function object. Returns the 
    function to use the @ notation'''
    def wrapper(func):
        func.parlength=paramslength
        return func
    return wrapper
    
############# below the base line and peak functions can be defined: #########
    
# the peak and baseline function definitions must be preceeded by a call of 
# the decorator that defines the length of the params array:
@setparameternum(2)
def lin(params, x):
    '''Returns values of linear function with 2 parameters: 
        params[0]+params[1]*x'''
    return params[0]+params[1]*x


@setparameternum(3)
def gaussamp(params, x):
    '''Return value of gaussian at x, using 3 parameters. x may be numpy.array.
    params =[h, xpos, w], with:
        h: height, amplitude 
        xpos: position
        w: width (sigma)
    Note: The area under the curve can be calculated by: 
        A= h*w*sqrt(2*pi)'''
    h = params[0]
    xpos = params[1]
    w = params[2]
    
    f = h*exp(-np.power((x-xpos),2)/(2*np.power(w,2)))
    return f

@setparameternum(3)
def gaussarea(params, x):
    ''' Returns values of gaussian at x, using 3 parameters 
    params=[A, xpos, fwhm]:
        A: peak area 
        xpos: position
        fwhm: full width at half maximum.
        '''
    A = params[0]
    xpos = params[1]
    fwhm = params[2]
    f = (A * exp(((-4)*log(2)*np.power((x-xpos),2))/np.power(fwhm,2)) /
        (fwhm*sqrt(pi/(4*log(2)))))
    return f

@setparameternum(3)
def lorentzamp(params, x):
    ''' Returns values of lorentzian at x, using 3 parameters 
    params=[h, xpos, fwhm]:
        h: height, amplitude 
        xpos: position
        fwhm: full width at half maximum.
        '''
    h = params[0]
    xpos = params[1]
    fwhm = params[2]
    f = h/(1+4*np.power(((x-xpos)/fwhm),2))
    return f

@setparameternum(3)
def lorentzarea(params, x):
    ''' Returns values of lorentzian at x, using 3 parameters 
    params=[A, xpos, fwhm]:
        A: peak area
        xpos: position
        fwhm: full width at half maximum.
        '''
    A = params[0]
    xpos = params[1]
    fwhm = params[2]
    f = ((2*A/(pi*fwhm)) *
        1/(1+4*np.power(((x-xpos)/fwhm),2)))
    return f

    
#%% #################### here follows some code for testing ##################
if __name__=='__main__':  
    import specage.ir_readers as ir
    import matplotlib.pyplot as plt
    File = 'C://Zmirror//smartCC//Bruker IR//18_07_10//18_07_10_MR51b.0001.dpt'
    sp=ir.bruk_dpt(File)
    lim=[1620, 1690]
    sp.set_peaklimit(lim)
    
    fitparbool = np.bool8([ 1,1,
                            1,1,1,
                            1,1,1,
                            1,1,1,
                            1,1,1,
                           ])
    
    fp = [1620, -0.00622]# fixing point for initial guess baseline [x1,y1]
    m= 0.5E-6
    n= fp[1]-m*fp[0]
    initpara = np.array([n, m,
                          0.1E-3, 1630, 3,
                         0.5E-3, 1642, 5,
                         2.7E-3, 1660, 5,
                          1E-3, 1675, 5,
                         ])
    lb = np.array([-1,   -1,
                    0, 1610, 2,  
                    0, 1630.0, 2,
                    0, 1655, 2,
                    0, 1670, 2,
                    ])
    ub = np.array([      1,    1,
                    np.inf, 1640, 25,   
                    np.inf, 1650, 25,
                    np.inf, 1665, 25,
                    np.inf, 1680, 25,
                    ])
    
    model = [lin, 
             gaussarea, 
             gaussarea, 
             gaussarea,
             gaussarea,
             ]
             
    
    initialguess = evalfitfuns(initpara, sp.get_peak()[:,0], model)
    
    plt.close('all')
    ax1 = plt.axes(title='Initial guess')
    ax1.plot(sp.get_peak()[:,0], sp.get_peak()[:,1],'o')
    #plot initialguess baseline:
    ax1.plot(sp.get_peak()[:,0], initialguess[:,0])
    #plot functions on baseline
    ax1.plot(sp.get_peak()[:,0], (initialguess[:,1:].T+initialguess[:,0]).T)
             
    #plot sum of peaks on baseline:
    ax1.plot(sp.get_peak()[:,0], initialguess[:,1:].sum(1)+initialguess[:,0])


    fitpeak(sp, initpara, fitparbool, model, 
                lb = lb, ub=ub, 
                verbose=1, fitblcorrected=False, 
                xtol=1E-15)
    
    plt.figure()
    ax2 = plt.axes(title='Result')
    ax2.plot(sp.get_peak()[:,0], sp.get_peak()[:,1],'o')
    #baseline 
    ax2.plot(sp.fittedpeaks.x, sp.fittedpeaks.intarray[:,0])
        #plot functions on baseline
    ax2.plot(sp.get_peak()[:,0], 
             (sp.fittedpeaks.intarray[:,1:].T+sp.fittedpeaks.intarray[:,0]).T)
             
    #plot sum of peaks on baseline:
    ax2.plot(sp.get_peak()[:,0], sp.fittedpeaks.intarray.sum(1))
    
    