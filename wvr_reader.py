# -*- coding: utf-8 -*-
"""
Functions for reading Output from MatLab Program 'WaterVapRemover' 
"""
#M.Rabe , 2018

import numpy as _np
from specage.ascii_magic import readASCII_table  as _readascii
from specage.spcl import Spectrum



def singleascii(Filename, blcorr = False):
    """Return Spectrum object from 'WaterVapRemover' ascii file. For 
    blcorr = true the baseline corrected spectrum is read"""
    
    datatable = _readascii(Filename, headlines=7, delim = None)
    wvl = _np.array(datatable[0][:,0])
    if blcorr:
        Abs = _np.array(datatable[0][:,2])
    else:
        Abs = _np.array(datatable[0][:,1])
        
    return Spectrum(wvl, Abs,FileName=Filename, ylabel='absorbance / au',
                         xlabel='\wavenumber / cm^{-1}')

def filelist(Filelist, blcorr = False):
    """Return list of Spectrum object from list of 'WaterVapRemover' ascii files"""
    Speclist=[]
    for File in Filelist:
        Speclist.append(singleascii(File, blcorr))
    return Speclist
