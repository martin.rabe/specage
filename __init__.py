# -*- coding: utf-8 -*-
#TODO: this must be updated

# import specage function libraries:
from . import ascii_magic
from . import spcl
from . import unit_convert
from . import pkfit
from . import piratr

# import specage readers:
from . import ov_readers
from . import ir_readers
from . import labram_readers
from . import peuvvis_readers
#from . import spc_readers  this raises an error when the spc module is not installed
from . import witec_readers
from . import wvr_reader
