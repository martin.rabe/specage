#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 10:08:23 2018
Helpful functions for unit conversions.
@author: Martin Rabe
"""
import numpy as np
hc = 1.23984193 #[µm eV]
def nm2wavenum(nm):
    """Returns wavenumber [cm⁻¹] from wavelength [nm]"""
    wavenum = 1/(np.array(nm)*1E-7)
    return wavenum

def wavenum2nm(wavenum):
    """Returns wavelength [nm] from wavenumber [cm⁻¹] """
    nm = 1/(np.array(wavenum)*1E-7)
    return nm

def nm2eV(nm):
    """Returns energy [eV] from wavelength [nm] """
    eV = hc / (np.array(nm)*1E-3)
    return eV

def eV2nm(eV):
    """Returns wavelength [nm] from energy [eV]"""
    nm = hc / (np.array(eV)*1E-3)
    return nm