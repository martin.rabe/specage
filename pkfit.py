#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# TODO: complete doc, explain usage
"""
Peak fitting interface. New implementation that should be used in stead of 
specage.peakfit or specage.pkfit_cf. scipy.optimize.curve_fit is used and 
individual models can be created. linear baselines, lorentzian and gaussian 
peak shapes are implemented, yet. Further functions can be easily implemented 
if needed. Furthermore global fitting of spectral series with shared parameters 
is fully supported, now.

Using curve_fit however also has some disadvantages. For instance not all 
information about the optimization is returned. Also, it uses different 
functions depending on which method is used (lm or trf) and hence sometimes 
one might obtain a keyword argument error when **kwargs are used.There is an 
older implementation that uses the scipy.optimize.least_squares function 
implemented in pkfit_ls which might be useful if these issues are problematic 
however this does not allow global fitting.

Usage:
    ... needs to be explained ...

@author: MR, 2022
"""

import numpy as np
from numpy import log, exp, pi, sqrt, inf
from scipy.optimize import curve_fit as cf
from types import SimpleNamespace as sns
from warnings import warn
from scipy.special import wofz, voigt_profile

import specage.spcl as spcl
from specage.spcl import SpectralSeries as spcser
from specage.spcl import Spectrum as spc


def fitpeak(specageobj, initpara, model, xrange=[], 
            fitblcorrected = False, fitglobal = False, **kwargs):
    ''' fit spectra with shared parameter'''
    
    # first check the basics of the input data:
    
    if isinstance(specageobj, spc):
        series = [specageobj]
    
    elif isinstance(specageobj, spcser) or isinstance(specageobj, 
                                                      spcl.TimeSeries):
        if fitglobal:
            series = specageobj
        else:
            #when no global fit is needed fitting might be faster
            if any(isshared(initpara)):
                warn('Shared parameters are obsolete when fitting a series with "fitglobal = False" and are hence treated as single parameters.', stacklevel=2)
            
            res =[]
            err = []
            for spec in specageobj:
               r,e = fitpeak(spec, initpara, model, xrange=xrange, 
                             fitblcorrected = fitblcorrected, 
                             fitglobal = fitglobal, **kwargs ) 
               res += [r.squeeze()]
               err += [e.squeeze()]                    
            return np.array(res), np.array(err)
    else:
        raise(TypeError(
            'specageobj must be Spectrum, TimeSeries, or SpectralSeries'))
    
    if initpara.dtype.type is not np.object_:
        raise(TypeError('initpara must be numpy.array with dtype=object'))
    
    if sum([func.parlength for func in model]) != len(initpara):
        raise(ValueError('Number of initial parameters does not match the model.'))
    
    fitparbool=np.bool8(ispara(initpara))
    
    # create all the arrays we need
    nrds = len(series)
    
    # determine the unique shared parameters
    usharedpara = []
    for param in initpara:
        # compare wether the current param is shared and has already been 
        # written to usharedpara:
        if isshared(param) and not any(
                [listedparam is param for listedparam in usharedpara]):
            usharedpara += [param]
    nrshpas = len(usharedpara)
    
    xlst = []
    ylst = []
    
    for spectrum in series:
        # set the x range. it maybe that peaklimits are already set in the 
        # spectrum and no xrange is provided in the function call: 
        if xrange != []:
            spectrum.set_peaklimit(xrange)
        else:
            try:
                spectrum.get_peak()
            except AttributeError:
                raise Exception(
                    'No x-range provided, neither in the spectrum nor in the function call')
        
        # there maybe problems if the type is complex numbers thus first
        if np.isreal(spectrum.get_peak()).all():
             peak = np.real(spectrum.get_peak())
        else:
            raise TypeError(
                'The Peak contains complex values. Fitting is not feasible')
        
        
        xlst += [peak[:,0]]
        
        if fitblcorrected:
            ylst += [peak[:,1]-peak[:,2]]
        else:
            ylst += [peak[:,1]]
        
    freeinitpara, fixedpara, shpamasks, fitshpamask=deconstrparvec(initpara, 
                                                                   fitparbool, 
                                                                   usharedpara)
    

    freeparalb = np.array([para.lb for para in freeinitpara])
    freeparaub = np.array([para.ub for para in freeinitpara])

    usharedparalb = np.array([para.lb for para in usharedpara])
    usharedparaub = np.array([para.ub for para in usharedpara])
    
    fullinitpara = constrfullpara(freeinitpara, usharedpara, nrds)
    fulllb = constrfullpara(freeparalb, usharedparalb, nrds)
    fullub = constrfullpara(freeparaub, usharedparaub, nrds)
   
    xarr=np.array(xlst)
    xflat = xarr.flatten()
    
    yarr=np.array(ylst)
    yflat = yarr.flatten()
    
    objectivefun = returnobjectivefunshared(model, nrds, fitparbool, shpamasks, 
                                      fitshpamask, nrshpas, fixedpara)
    #default values: ftol=1e-8, xtol=1e-8
    result, pcov = cf(objectivefun, 
                      xflat, 
                      yflat,
                      p0=fullinitpara,
                      bounds = (fulllb, fullub),
                      **kwargs)
    
    resultparar = constrparar(result, nrshpas, nrds, fixedpara, 
                              shpamasks, fitshpamask, fitparbool)
    
    
    errors=np.sqrt(np.diag(pcov))
    fixedparaerrors = np.zeros(fixedpara.shape)
    fixedparaerrors[:] = np.nan
    errorparar = constrparar(errors, nrshpas, nrds, fixedparaerrors, 
                             shpamasks, fitshpamask, fitparbool)
    
    
    for spec, resultpar, err in zip(series, resultparar, errorparar):
        fitresult = sns()
        
        fitresult.parameters = np.copy(resultpar)
        fitresult.errors = err
        
        fitresult.input=sns()
        fitresult.input.model = model
        fitresult.input.initpara = initpara
        fitresult.input.fitblcorrected = fitblcorrected
        fitresult.input.usharedpara = usharedpara
        fitresult.input.xrange = xrange
        fitresult.input.kwargs = kwargs
        
        # create new specage.SpectralSeries from the results:
        peaks =  evaluateflatseries([resultpar], peak[:,0], 1, model)
        peaklist=[]
        for intensity in peaks.T:
            peaklist.append(spc(spec.get_peak()[:,0], intensity))
        
        PeakSeries =spcser(peaklist,np.arange(peaks.shape[1]),cName = 'number')


        spec.set_peakfitresults(PeakSeries, fitresult)
        
    return resultparar, errorparar

def returnobjectivefunshared(model, nrds, fitparbool, shpamasks, 
                             fitshpamask, nrshpas, fixedpara):
    '''returns the objective function, that can be used as input for curve_fit'''
    def evalfitfuns_fp(x, *fitparams):
        '''objective function, that can be used as input for curve_fit. 
        *fitparams starts with shared parameters for all datasets, followed by 
        single parameters for each data set.'''
        
        paras = constrparar(fitparams, nrshpas, nrds, fixedpara, 
                            shpamasks, fitshpamask, fitparbool)
        
        return evaluateflatseries(paras, x, nrds, model).sum(1)
    
    return evalfitfuns_fp

def constrparar(fitparams, nrshpas, nrds, fixedpara, shpamasks, fitshpamask, fitparbool):
    '''Returns the array of parameters that is needed for the input to the
    evaluateflatseries() function. The shared, single, and fixed parameters are 
    combined to give a full parameterset with one line of parameters per data set'''
    shpas = fitparams[0:nrshpas]
    frpas = np.array(fitparams[nrshpas:])
    frpas = frpas.reshape((nrds, int(len(frpas)/nrds)))
    paras = []
    for freepara in frpas:
        paras += [reconstrparvec(shpas, freepara, fixedpara, shpamasks, fitshpamask, fitparbool)]
    return np.array(paras)

def constrfullpara(freepara, usharedpara, nrds):
    '''Returns the input parameter vector for the objective function'''
    allfreeparas = []
    for nr in range(nrds):
        allfreeparas += [freepara]
    allfreeparas = np.array(allfreeparas).flatten()
    return np.append(usharedpara, allfreeparas)    

def deconstrparvec(params, fitparbool, usharedpara):
    fixedpara = params[~fitparbool]
    allfitpara = params[fitparbool]
    fitshpamask = np.bool8(isshared(allfitpara))
    frpamask = ~fitshpamask
    freepara = allfitpara[frpamask]
    
    # now find out which parameters are shared, they must be identical
    shpamasks =[]
    for para in usharedpara:
        shpamasks += [[para is element for element in allfitpara]]
    
    
    shpamasks=np.array(shpamasks)
    return freepara, fixedpara, shpamasks, fitshpamask
        
    
    
def reconstrparvec(usharedpara, freepara, fixedpara, 
                  shpamasks, fitshpamask, fitparbool):
    
    ne=np.zeros(len(fitshpamask), dtype=object)
    ne[:] = np.nan
    
    for mask, shpa in zip(shpamasks, usharedpara):
        ne[mask] = shpa
    ne[~fitshpamask] = freepara
    
    fullprec=np.ones(fitparbool.shape, dtype=object)
    fullprec[:]=np.nan
    fullprec[fitparbool] = ne
    fullprec[~fitparbool] = fixedpara

    return fullprec




def evaluateflatseries(paraarray, xflat, nrds, funlist):
    xarr=xflat.reshape((nrds, int(len(xflat)/nrds)))
    fullresultlst =[]
    for x, params in zip(xarr, paraarray):
        fullresultlst += [evalfitfuns(params, x, funlist)]
    
    fullresult = np.array(fullresultlst)
    return fullresult.reshape((fullresult.shape[0] * fullresult.shape[1],
                               fullresult.shape[2]))
    
def evalfitfuns(params, x, funlist):
    '''Evaluates single peak model and returns seperated results.'''
    indx = 0
    result=np.zeros((len(x), len(funlist)))
    for funnr, fun in enumerate(funlist):
        result[:,funnr] = fun(params[indx:indx+fun.parlength],x)
        indx += fun.parlength
    return result

def detushrpas(initpara):
    '''return the unique shared parameters'''
    uniquesharedparas = []
    for param in initpara:
        # compare wether the current param is shared and has already been 
        # written to uniqsharedparas:
        if isshared(param) and not any(
                [listedparam is param for listedparam in uniquesharedparas]):
            uniquesharedparas += [param]
    return uniquesharedparas

class sharedpar(float):
    '''Shared parameters are shared during fitting. They are a subclass of 
    floats. Parameters contain their lower and upper bounds which default to 
    -inf and +inf respectively.'''
    
    def __new__(cls, value, lb = -inf, ub = inf):
        if not (lb < value) or not (value<ub):
            raise ValueError(
                'Lower bound must be less than value which must be less than upper bound')
        return super().__new__(cls, value)
        
    def __init__(self, value, lb = -inf, ub = inf):   
        self.__shared__ = True
        self.__para__ = True
        self.lb = float(lb)
        self.ub = float(ub)

class singlepar(float):
    '''Single parameters are not shared during fitting. They are a subclass of 
    floats. Parameters contain their lower and upper bounds which default to 
    -inf and +inf respectively.'''
        
    def __new__(cls, value, lb = -inf, ub = inf):
        if not (lb < value) or not (value<ub):
            raise ValueError(
                'Lower bound must be less than value and value must be less than upper bound.')
        return super().__new__(cls, value)
    def __init__(self, value, lb = -inf, ub = inf):   
        self.__shared__ = False
        self.__para__ = True
        self.lb = float(lb)
        self.ub = float(ub)
    
    

def isshared(variable):       
    def testit(element):
        try:
            if element.__shared__ == True:
                return True
            else:
                return False
        except AttributeError:
            return False
    
    try:
        return [testit(element) for element in variable]
    except TypeError:
        return testit(variable)


def ispara(variable):       
    def testit(element):
        try:
            if element.__para__ == True:
                return True
            else:
                return False
        except AttributeError:
            return False
    
    try:
        return [testit(element) for element in variable]
    except TypeError:
        return testit(variable)



def setparameternum(paramslength):
    '''Function decorator that allows to store the required length 
    of the parameter vector as attribute of the function object. Returns the 
    function to use the @ notation'''
    def wrapper(func):
        func.parlength=paramslength
        return func
    return wrapper
    
############# below the base line and peak functions can be defined: #########
    
# the peak and baseline function definitions must be preceeded by a call of 
# the decorator that defines the length of the params array:
@setparameternum(2)
def lin(params, x):
    '''Returns values of linear function with 2 parameters: 
        params[0]+params[1]*x'''
    return params[0]+params[1]*x


@setparameternum(3)
def gaussamp(params, x):
    '''Return value of gaussian at x, using 3 parameters. x may be numpy.array.
    params =[h, xpos, w], with:
        h: height, amplitude 
        xpos: position
        w: width (sigma)
    Note: The area under the curve can be calculated by: 
        A= h*w*sqrt(2*pi)'''
    h = params[0]
    xpos = params[1]
    w = params[2]
    
    f = h*exp(-np.power((x-xpos),2)/(2*np.power(w,2)))
    return f

@setparameternum(3)
def gaussarea(params, x):
    ''' Returns values of gaussian at x, using 3 parameters 
    params=[A, xpos, fwhm]:
        A: peak area 
        xpos: position
        fwhm: full width at half maximum.
        '''
    A = params[0]
    xpos = params[1]
    fwhm = params[2]
    f = (A * exp(((-4)*log(2)*np.power((x-xpos),2))/np.power(fwhm,2)) /
        (fwhm*sqrt(pi/(4*log(2)))))
    return f

@setparameternum(3)
def lorentzamp(params, x):
    ''' Returns values of lorentzian at x, using 3 parameters 
    params=[h, xpos, fwhm]:
        h: height, amplitude 
        xpos: position
        fwhm: full width at half maximum.
        '''
    h = params[0]
    xpos = params[1]
    fwhm = params[2]
    f = h/(1+4*np.power(((x-xpos)/fwhm),2))
    return f

@setparameternum(3)
def lorentzarea(params, x):
    ''' Returns values of lorentzian at x, using 3 parameters 
    params=[A, xpos, fwhm]:
        A: peak area
        xpos: position
        fwhm: full width at half maximum.
        '''
    A = params[0]
    xpos = params[1]
    fwhm = params[2]
    f = ((2*A/(pi*fwhm)) *
        1/(1+4*np.power(((x-xpos)/fwhm),2)))
    return f

@setparameternum(4)
def voigt(params, x):
    ''' Returns values of the Voigt profile at x, using 4 parameters
    params = [A, xpos, alpha, gamma]:
        A: peak area
        xpos: position
        alpha: half width at half maximum of the Gaussian profile
        gamma: half width at half maximum of the Lorentzian profile
    Note: The Voigt profile is the convolution of a Gaussian and a Lorentzian 
    profile. For alpha = 0 or gamma = 0 the function returns the pure 
    Lorentzian or Gaussian profile, respectively.
    '''
    A = params[0]
    xpos = params[1]
    alpha = params[2]
    gamma = params[3]
    sigma = alpha / np.sqrt(2 * np.log(2))
    # return A*(np.real(wofz(((x-xpos) + 1j*gamma)/sigma/np.sqrt(2))) / 
           # (sigma *np.sqrt(2*np.pi))) #this definition could yield to division by 0 for alpha =0
    return A*voigt_profile(x-xpos, sigma, gamma) #this function works for alpha = 0   


    
#%% #################### here follows some code for testing ##################
if __name__=='__main__':  
    import specage.ir_readers as ir
    import matplotlib.pyplot as plt
    File = 'C://Zmirror//smartCC//Bruker IR//18_07_10//18_07_10_MR51b.0001.dpt'
    sp=ir.bruk_dpt(File)
    lim=[1620, 1690]
    sp.set_peaklimit(lim)
    
    model = [lin, 
             gaussarea, 
             gaussarea, 
             voigt,
             gaussarea,
             ]
    
    fp = [1620, -0.00622]# fixing point for initial guess baseline [x1,y1]
    m= singlepar(0.5E-6)
    n= singlepar(fp[1]-m*fp[0])
    
    a1 = singlepar(0.1E-3, lb=0)
    x1 = singlepar(1630)
    w1 = singlepar(10, lb=5)
    
    a2 = singlepar(0.5E-3, lb=0)
    x2 = singlepar(1642)
    w2 = sharedpar(10, lb=5)
    
    a3 = singlepar(2.7E-3, lb=0)
    x3 = singlepar(1660)
    alpha3 = 5
    gamma3 = singlepar(5)
    # w3 = singlepar(10, lb=5)
    
    a4 = singlepar(1E-3, lb=0)
    x4 = singlepar(1675)
    # w4 = sharedpar(10, lb=5)
    
   
    initpara = np.array([ n, m,
                         a1,x1,w1,
                         a2,x2,w2,
                         a3,x3,alpha3,gamma3,
                         a4,x4,w2,
                                 ],
                        dtype=object)
    
    initialguess = evalfitfuns(initpara, sp.get_peak()[:,0], model)
    
    plt.close('all')
    ax1 = plt.axes(title='Initial guess')
    ax1.plot(sp.get_peak()[:,0], sp.get_peak()[:,1],'o')
    #plot initialguess baseline:
    ax1.plot(sp.get_peak()[:,0], initialguess[:,0])
    #plot functions on baseline
    ax1.plot(sp.get_peak()[:,0], (initialguess[:,1:].T+initialguess[:,0]).T)
             
    #plot sum of peaks on baseline:
    ax1.plot(sp.get_peak()[:,0], initialguess[:,1:].sum(1)+initialguess[:,0])

    
    fitpeak(sp, initpara, model, verbose=2, 
            fitblcorrected=False, xtol=1E-15)
    
    plt.figure()
    ax2 = plt.axes(title='Result')
    ax2.plot(sp.get_peak()[:,0], sp.get_peak()[:,1],'o')
    #baseline 
    ax2.plot(sp.fittedpeaks.x, sp.fittedpeaks.intarray[:,0])
        #plot functions on baseline
    ax2.plot(sp.get_peak()[:,0], 
             (sp.fittedpeaks.intarray[:,1:].T+sp.fittedpeaks.intarray[:,0]).T)
             
    #plot sum of peaks on baseline:
    ax2.plot(sp.get_peak()[:,0], sp.fittedpeaks.intarray.sum(1))
    
    